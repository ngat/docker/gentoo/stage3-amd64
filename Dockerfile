FROM gentoo/stage3-amd64

LABEL maintainer="colton@nextgenagritech.com" \
      "com.nextgenagritech.vendor"="NextGen AgriTech" \
      version="1.0.0"

RUN echo " --> Update for Docker" \
    && sed -i -e 's/#rc_sys=""/rc_sys="docker"/g' /etc/rc.conf \
    && echo 'UTC' > etc/timezone \
    && echo " --> Running Emerge Web Synchronization" \
    && emerge-webrsync \
    && echo " --> Make emerge configuration directories" \
    && mkdir -p /etc/portage/package.use \
    && mkdir -p /etc/portage/package.accept_keywords \
    && mkdir -p /etc/portage/package.unmask \
    && mkdir -p /etc/portage/repos.conf \
    && mkdir -p /etc/portage/profile \
    && echo " --> Create layman package.use file" \
    && echo "app-portage/layman sync-plugin-portage" > /etc/portage/package.use/layman \
    && echo " --> Configuring and building locales" \
    && echo "en_US ISO-8859-1" >> /etc/locale.gen \
    && echo "en_US.UTF-8 UTF-8" >> /etc/locale.gen \
    && locale-gen \
    && echo " --> Update portage" \
    && emerge -j6 -v1 portage \
    && echo " --> Updating world packages" \
    && emerge -j6 -vuND @world perl-cleaner help2man gentoolkit layman vim \
    && echo " --> Running Perl Cleaner" \
    && perl-cleaner --all \
    && echo " --> Running @preserved-rebuild" \
    && emerge -j6 -v @preserved-rebuild \
    && echo " --> Update layman check official to no" \
    && sed -i -e 's/check_official : Yes/check_official : No/g' /etc/layman/layman.cfg \
    && echo " --> Running revdep-rebuild" \
    && revdep-rebuild \
    && echo " --> Tell openrc loopback and net are already there, since docker handles the networking" \
    && echo 'rc_provide="loopback net"' >> /etc/rc.conf \
    && echo " --> no need for loggers" \
    && sed -i 's/^#\(rc_logger="YES"\)$/\1/' /etc/rc.conf \
    && echo " --> can't get ttys unless you run the container in privileged mode" \
    && sed -i '/tty/d' /etc/inittab \
    && echo " --> can't set hostname since docker sets it" \
    && sed -i 's/hostname $opts/# hostname $opts/g' /etc/init.d/hostname \
    && echo " --> can't mount tmpfs since not privileged" \
    && sed -i 's/mount -t tmpfs/# mount -t tmpfs/g' /lib/rc/sh/init.sh \
    && echo " --> can't do cgroups" \
    && sed -i 's/cgroup_add_service /# cgroup_add_service /g' /lib/rc/sh/openrc-run.sh \
    && echo " --> Fixing openrc run folder" \
    && rm /run/openrc \
    && mkdir /run/openrc \
    && echo " --> Removing unnecessary sysinit tasks" \
    && rc-update del udev sysinit \
    && rc-update del udev-trigger sysinit \
    && rc-update del kmod-static-nodes sysinit \
    && echo " --> Cleaning up" \
    && rm -fr /usr/portage/* \
    && rm -fr /tmp/* \
    && rm -fr /var/tmp/*
    #&& rm -fr /usr/share/man/* \
    #&& rm -fr /usr/share/sgml/* \
    #&& rm -fr /usr/share/doc/* \
    #&& rm -fr /usr/share/gtk-doc/*

CMD [ "/sbin/init" ]
