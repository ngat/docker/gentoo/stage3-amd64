Gentoo Docker Images
====================

A docker image built on top of [stage3-amd64](https://hub.docker.com/r/gentoo/stage3-amd64/) which is configured to boot using OpenRC init.

Build Commands
==============
```docker
RUN echo " --> Running Emerge Web Synchronization" \
    && emerge-webrsync \
    && echo " --> Make emerge configuration directories" \
    && mkdir -p /etc/portage/package.use \
    && mkdir -p /etc/portage/package.accept_keywords \
    && echo " --> Configuring and building locales" \
    && echo "en_US ISO-8859-1" >> /etc/locale.gen \
    && echo "en_US.UTF-8 UTF-8" >> /etc/locale.gen \
    && locale-gen \
    && echo " --> Updating world packages" \
    && emerge -j6 -vuND @world perl-cleaner help2man \
    && echo " --> Running Perl Cleaner" \
    && perl-cleaner --all \
    && echo " --> Tell openrc loopback and net are already there, since docker handles the networking" \
    && echo 'rc_provide="loopback net"' >> /etc/rc.conf \
    && echo " --> no need for loggers" \
    && sed -i 's/^#\(rc_logger="YES"\)$/\1/' /etc/rc.conf \
    && echo " --> can't get ttys unless you run the container in privileged mode" \
    && sed -i '/tty/d' /etc/inittab \
    && echo " --> can't set hostname since docker sets it" \
    && sed -i 's/hostname $opts/# hostname $opts/g' /etc/init.d/hostname \
    && echo " --> can't mount tmpfs since not privileged" \
    && sed -i 's/mount -t tmpfs/# mount -t tmpfs/g' /lib/rc/sh/init.sh \
    && echo " --> can't do cgroups" \
    && sed -i 's/cgroup_add_service /# cgroup_add_service /g' /lib/rc/sh/openrc-run.sh \
    && echo " --> Fixing openrc run folder" \
    && rm /run/openrc \
    && mkdir /run/openrc \
    && echo " --> Removing unnecessary sysinit tasks" \
    && rc-update del udev sysinit \
    && rc-update del udev-trigger sysinit \
    && rc-update del kmod-static-nodes sysinit \
    && echo " --> Cleaning up" \
    && emerge --depclean --with-bdeps=n \
    && rm -fr /usr/portage/* \
    && rm -fr /tmp/* \
    && rm -fr /var/tmp/*
    #&& rm -fr /usr/share/man/* \
    #&& rm -fr /usr/share/sgml/* \
    #&& rm -fr /usr/share/doc/* \
    #&& rm -fr /usr/share/gtk-doc/*
```

How To Use
==========

## Running without init
docker run --rm -it registry.gitlab.com/ngat/docker/gentoo/stage3-amd64 /bin/bash

## Running in background

  1. docker create --name $SOME_NAME registry.gitlab.com/ngat/docker/gentoo/stage3-amd64
  2. docker start $SOME_NAME
  3. docker exec -it $SOME_NAME /bin/bash

## Using Dockerfile

```docker
FROM registry.gitlab.com/ngat/docker/gentoo/stage3-amd64
RUN emerge-webrsync \
    && echo " --> Installing $YOUR_PACKAGES" \
    && emerge -j6 -v $YOUR_PACKAGES \
    ... Add your commands here ...
    && echo " --> Cleaning up" \
    && emerge --depclean --with-bdeps=n \
    && rm -fr /usr/portage/* \
    && rm -fr /tmp/* \
    && rm -fr /var/tmp/*
    #&& rm -fr /usr/share/man/* \
    #&& rm -fr /usr/share/sgml/* \
    #&& rm -fr /usr/share/doc/* \
    #&& rm -fr /usr/share/gtk-doc/*
CMD [ "/sbin/init" ]
```
  1. Create Dockerfile
  2. If need package.use customizations
    1. Create package.use directory
    2. Add files to package.use directory
    3. Add `ADD package.use/* /etc/portage/package.use/` to Dockerfile before RUN
  3. If need package.accept_keywords customizations
    1. Create package.accept_keywords directory
    2. Add files to package.accept_keywords directory
    3. Add `ADD package.accept_keywords/* /etc/portage/package.accept_keywords/` to Dockerfile before RUN
